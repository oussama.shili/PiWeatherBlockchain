var mongoose = require('mongoose');
var User = require('../models/user')
var InstagramOutfitDataSchema = mongoose.Schema({
    clothes: { type: String },
    user: { type: mongoose.Schema.ObjectId, ref: 'User' }
}, { collection: "InstagramOutfitDataCustom" });

module.exports = mongoose.model("InstagramOutfitData", InstagramOutfitDataSchema);