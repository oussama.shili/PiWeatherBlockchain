var mongoose = require('mongoose');
var User = require('../models/user');
var valid = require('validator');


var stationSchema = mongoose.Schema({
  name: { type: String },
  description: { type: String },
  createdAt: { type: Date, default: Date(Date.now) },
  address: { type: String },
  lat: { type: Number },
  lng: { type: Number },
  working: { type: Boolean },
  createdBy: {
    type: mongoose.Schema.ObjectId,
    ref: 'User'
  }
}, { collection: "stationCustom" });

stationSchema.statics.findStationByUser = function (userId, callback) {
  var query = this.find()

  User.findOne({ '_id': userId }, function (error, user) {
    query.where(
      { createdBy: user._id }
    ).populate('createdBy').exec(callback);
  })
  return query
}

module.exports = mongoose.model("Station", stationSchema);