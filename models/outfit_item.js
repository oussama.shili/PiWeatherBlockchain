var mongoose = require('mongoose');

var outfitItemSchema = mongoose.Schema({
    name: { type: String },
    minAge: { type: Number },
    maxAge: { type: Number },
    sex: { type: String },
    minTemperature: { type: Number },
    maxTemperature: { type: Number },
    activities: { type: String },
    styles: { type: String },
    photo: { type: String }
}, { collection: "outfitItemCustom" });

module.exports = mongoose.model("OutfitItem", outfitItemSchema);