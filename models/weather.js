var mongoose = require('mongoose');
var Station = require('../models/station');

var weatherSchema = mongoose.Schema({
    temperature: { type: Number },
    humidity: { type: Number },
    addedAt: { type: Date },
    stationConcerned: {
        type: mongoose.Schema.ObjectId,
        ref: 'Station'
    }
}, { collection: "weatherCustom" });

weatherSchema.statics.getWeatherByStation = function (stationId, callback) {
    var query = this.find()

    Station.findOne({ '_id': stationId }, function (error, station) {
        query.where(
            { stationConcerned: station._id }
        ).populate('stationConcerned').exec(callback);
    })
    return query
}


module.exports = mongoose.model("Weather", weatherSchema);