var bcrypt = require('bcrypt-nodejs');
var Q = require('q');
var User = require('../models/user');
//used in local-signup strategy
exports.localReg = function (username, password) {
  var deferred = Q.defer();

    //check if username is already assigned in our database
    User.findOne({'username' : username})
      .then(function (result) {
        if (null != result) {
          console.log("USERNAME ALREADY EXISTS:", result.username);
          deferred.resolve(false); // username exists
        }
        else  {
          var hash = bcrypt.hashSync(password, 8);
          var user = {
            "username": username,
            "password": hash,
            "avatar": "http://placepuppy.it/images/homepage/Beagle_puppy_6_weeks.JPG"
          }

          console.log("CREATING USER:", username);
        
          collection.insert(user)
            .then(function () {
              deferred.resolve(user);
            });
        }
      });

  return deferred.promise;
};


//check if user exists
    //if user exists check if passwords match (use bcrypt.compareSync(password, hash); // true where 'hash' is password in DB)
      //if password matches take into website
  //if user doesn't exist or password doesn't match tell them it failed
exports.localAuth = function (email, password) {
  var deferred = Q.defer();

  console.log('in localAuth');
    User.findOne({'email' : email})
      .then(function (result) {
        if (null == result) {
          console.log("email NOT FOUND:", email);

          deferred.resolve(false);
        }
        else {
          var hash = result.password;

          console.log("FOUND USER: " + result.email);

          if (bcrypt.compareSync(password, hash)) {
            deferred.resolve(result);
          } else {
            console.log("AUTHENTICATION FAILED");
            deferred.resolve(false);
          }
        }

      });
 

  return deferred.promise;
}
