import os
import sys
import json
import base64
import tarfile
import pymongo
from pymongo import MongoClient 

import deepomatic
from deepomatic import ImageInput

if sys.version_info >= (3, 0):
    from urllib.request import urlretrieve
else:
    from urllib import urlretrieve

if len(sys.argv) < 2:
    api_host = None
else:
    api_host = sys.argv[1]

app_id = os.getenv('650689659321')
api_key = os.getenv('3c2c3bcbcf534e6580b2d897d45dc62f')
client = deepomatic.Client('650689659321', '3c2c3bcbcf534e6580b2d897d45dc62f', host=api_host)
def start():
    data_list = []
    outfile = open('./deepomatic-client-python/data.json', 'a+')
    outfile.write('{"data":')
    with open('./deepomatic-client-python/instagram.json', encoding='utf-8') as json_file:  
        data = json.load(json_file)
        for p in data['urls']:
            demo_url = p['src'] 
            def demo():
                result = client.Network.list(public=True).data()
                network = client.Network.retrieve('imagenet-inception-v1')               
                print_header("Inference from a URL")
                result = network.inference(inputs=[ImageInput(demo_url)], output_tensors=["prob", "pool2/3x3_s2", "pool5/7x7_s1"])
                display_inference_tensor(result)
                  
                def pretty_print_json(result):
                    data_list.append(result)
               
                spec = client.RecognitionSpec.retrieve('imagenet-inception-v1')

                print_header("Infering an image on recognition spec current version")

                result = spec.inference(inputs=[ImageInput(demo_url)], show_discarded=True, max_predictions=3)
                pretty_print_json(result)

                preprocessing = {
                    "inputs": [
                        {
                            "tensor_name": "data",
                            "image": {
                                "color_channels": "BGR",
                                "target_size": "224x224",
                                "resize_type": "SQUASH",
                                "mean_file": "mean.binaryproto",
                                "dimension_order": "NCHW",
                                "pixel_scaling": 255.0,
                                "data_type": "FLOAT32"
                            }
                        }
                    ],
                    "batched_output": True
                }
            demo()
    outfile.write(json.dumps(data_list))                    
    outfile.write('}') 
           
###########
# Helpers #
###########

def download(url, local_path):
    if not os.path.isfile(local_path):
        print("Downloading {} to {}".format(url, local_path))
        urlretrieve(url, local_path)
        if url.endswith('.tar.gz'):
            tar = tarfile.open(local_path, "r:gz")
            tar.extractall(path='/tmp/')
            tar.close()
    else:
        print("Skipping download of {} to {}: file already exist".format(url, local_path))
    return local_path


def print_header(text):
    print("\n{}".format(text))


def print_comment(text):
    print("--> " + text)


def pretty_print_json(data):
    print(json.dumps(data, indent=4, separators=(',', ': ')))


def display_inference_tensor(result):
    for tensor_name, numpy_array in result.items():
        print_comment("tensor '{name}', dimensions: {dims}".format(name=tensor_name, dims='x'.join(map(str, numpy_array.shape))))


if __name__ == '__main__':
    start()
