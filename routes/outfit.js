var express = require('express');
var router = express.Router();
var session = require('express-session');
var PythonShell = require('python-shell');
var instagram_outfit_data = require('../models/instagram_outfit_data');
var User = require('../models/user');
var fs = require('fs');
var ig = require('instagram-node').instagram();
var httpRequest = require('request');
var passport = require('passport');

var accessToken;
var data_list = [];

ig.use({
    client_id: '57be76a9b56b478599aea8e627cc8206',
    client_secret: 'fc12f7d66a764c09a766f64072bda125',
});

var redirect_uri = 'http://localhost:3000/outfit/handleauth';

router.get('/', function(req, res, next) {
    console.log(session.user);
    //res.render('index');
});

router.get('/outfitService', function(req, res, next) {

    console.log("token: " + accessToken);
    ig.use({
        access_token: accessToken
    });

    var options = {
        url: 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' + accessToken,
        method: 'GET'
    };
    httpRequest(options, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            fs.writeFileSync("pre_instagram.json", body);
            var test = require("../pre_instagram.json");
            var user = new User()
            console.log(JSON.stringify(test.pagination))

            fs.writeFileSync("./deepomatic-client-python/instagram.json", '{"urls": [');
            for (var i = 0, len = test.data.length; i < len; i++) {
                fs.appendFileSync("./deepomatic-client-python/instagram.json", '{"src":');
                fs.appendFileSync('./deepomatic-client-python/instagram.json', JSON.stringify(test.data[i]["images"]["standard_resolution"]["url"]));
                if (i < len - 1) {
                    fs.appendFileSync("./deepomatic-client-python/instagram.json", '},');
                } else {
                    fs.appendFileSync("./deepomatic-client-python/instagram.json", '}');
                }
            }
            fs.appendFileSync("./deepomatic-client-python/instagram.json", ']}');
            console.log("get test");
            PythonShell.run('./deepomatic-client-python/demo.py', function(err, success) {
                if (err) throw err;

                else if (success) {
                    var data_json = require('../deepomatic-client-python/data.json');
                    for (var i = 0, len = data_json.data.length; i < len; i++) {
                        for (var j = 0, lenth = data_json.data[i]["outputs"].length; j < lenth; j++) {
                            for (var k = 0, l = data_json.data[i]["outputs"][j]["labels"]["predicted"].length; k < l; k++) {
                                console.log(JSON.stringify(data_json.data[i]["outputs"][j]["labels"]["predicted"][k]["label_name"]))
                                var a = new instagram_outfit_data()
                                a.clothes = JSON.stringify(data_json.data[i]["outputs"][j]["labels"]["predicted"][k]["label_name"])

                                a.user = session.user._id;
                                a.save(function(err, result) {
                                    if (err) {
                                        res.send(err);
                                    }
                                })
                            }
                        }
                    }
                    fs.unlink('./deepomatic-client-python/instagram.json', function(err) {
                        if (err) throw err;
                    });
                    fs.unlink('./deepomatic-client-python/data.json', function(err) {
                        if (err) throw err;
                    });
                    console.log("kamell")
                    res.json({ "message": "instagram data ok" })

                }
            });
        }
    });
});


router.get('/outfitadviser', function(req, res, next) {
    api.user_media_recent('user_id', function(err, medias, pagination, remaining, limit) {
        console.log(medias);
    });
    res.render('outfitadviser');
});

router.get('/returnIndex', function(req, res) {});


router.get('/authorize', function(req, res) {
    // set the scope of our application to be able to access likes and public content
    return res.redirect(ig.get_authorization_url(redirect_uri, { scope: ['public_content', 'likes'] }));

});


router.get('/handleauth', function(req, res) {
    //retrieves the code that was passed along as a query to the '/handleAuth' route and uses this code to construct an access token
    ig.authorize_user(req.query.code, redirect_uri, function(err, result) {
        if (err) res.send(err);
        // store this access_token in a global variable called accessToken
        accessToken = result.access_token;
        // After getting the access_token redirect to the '/' route 
        res.redirect('http://localhost:8081/#/outfit');
    });
});


//module.exports = function (request) {
router.get('/test', function(req, res) {
    var options = {
        url: 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' + accessToken,
        method: 'GET',
        form: {
            client_id: '57be76a9b56b478599aea8e627cc8206',
            client_secret: 'fc12f7d66a764c09a766f64072bda125',
            grant_type: 'authorization_code',
            redirect_uri: redirect_uri,
            code: req.query.code
        }
    };
    httpRequest(options, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            var user = JSON.stringify(body);
            console.log(user)
        }
    });

});

module.exports = router;