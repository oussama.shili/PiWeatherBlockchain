var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt-nodejs');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var User = require('../models/user');


if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}

var connectedUser;

// Login
router.get('/login', function(req, res){
	console.log("ouh");
});

router.post('/instagramConnection', function (req, res) {
  console.log("1");
  let user = {};
  user.instagramConnected = true;

  console.log(session.user.email);
  let query = { email: session.user.email }

  User.update(query, user, function (err) {

    if (err) {
      console.log(err);
      return;
    } else {
      console.log('instagram connection')
      res.send("updated");
    }
  });

});
/* GET users listing. */
router.get('/', function (req, res, next) {
  User.find(function (err, users) {
    if (err) {
      res.json(err);
    }
    else {
      res.json(users);
    }
  })
});

/*router.post('/register', function (req, res) {
  var user = new User({
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10)),
    isTokenValid: false,
    accountAdresse: req.body.accountAdresse
  });

  user.save(function (err, result) {
    if (err) {
      return res.send(err);
    }

    console.log('user added');
    req.session.user = user;
    connectedUser = user;
    console.log(req.session.user);
    res.json(user);
  })
});*/

// Register User
router.post('/register', function(req, res){
//	var name = req.body.name;
  var email = req.body.email;
  var firstname = req.body.firstname;
  var lastname = req.body.lastname;
  var age = req.body.age;
	var sex = req.body.sex;
	var password = req.body.password;
	//var password2 = req.body.password2;

	// Validation
	//req.checkBody('name', 'Name is required').notEmpty();
/*	req.checkBody('email', 'Email is required').notEmpty();
	req.checkBody('email', 'Email is not valid').isEmail();
	req.checkBody('username', 'Username is required').notEmpty();
	req.checkBody('password', 'Password is required').notEmpty();
	//req.checkBody('password2', 'Passwords do not match').equals(req.body.password);*/

	//var errors = req.validationErrors();

	//if(errors){
	/*	res.render('register',{
			errors:errors
    });*/
   // console.log(errors);
//	} else {
		var newUser = new User({
		//	name: name,
      email:email,
      firstname: firstname,
      lastname: lastname,
      age: age,
      sex: sex,
      password: password,
		});

		User.createUser(newUser, function(err, user){
      if(err)
        {
            console.log("no")
            console.log(err);
            res.send("mail already exists");
        }
        else{
          console.log("user created !!!!!!!")
          console.log(user);
          res.json(user);
        }
    
		});

	//	req.flash('success_msg', 'You are registered and can now login');

  //res.json(user);
	//}
});
/*
router.post('/login', function (req, res,callback) {
  User.findOne({ email: req.body.email },
    function (err, user) {
      if (err) { res.send(err); }
      else if (bcrypt.compareSync(req.body.password, user.password) == false) {
        console.log('Invalid email or password');
        var err = new Error('User not found.');
        err.status = 401;
        res.json(err);
      }
    
        else if (bcrypt.compareSync(req.body.password, user.password)) {
          console.log('user connected');
          req.session.user = user;
          connectedUser = user;
          console.log(req.session.user);
          res.json(user);
        }
      
    })
});*/

//login
router.post('/login', function(req, res, next) {
  passport.authenticate('local', function (error, user, info){

      // A error also means, an unsuccessful login attempt
      if(error) {
          console.error(error);
          console.log('Failed login:');
          // And do whatever you want here.
          res.sendStatus(401);
      }

      if (user === false) {
          // handle login error ...
          console.log('Failed login:');
          res.sendStatus(401);
      } else {
        req.login(user, function(err) {
          if (err) {
             console.log(err);
          } else {
            console.log('Successful login:');
            session.user = user;
            console.log(req.user);
            res.json(user);
            //  mysend(res, 200, JSON.stringify(user));
          }
      }
          // handle successful login ...
        
         // session.user = user;
       /*  session.passport = user;
          connectedUser = user;
          //console.log(session.user);
         // console.log(session.user);
          console.log(session.passport);
          console.log('Successful login:');

          
          res.json(user);*/
        )}
  })(req, res, next);
});

router.get('/logout', function (req, res) {
  //req.session.user = null;
  req.logout();

	req.flash('success_msg', 'You are logged out');
  console.log('disconnected');
  res.json({ "message": "disconnected" })
});


/*router.post('/:id', function (req, res) {
  var id = req.params.id;
  User.updateOne({ '_id': id }, req.body, function (err, result) {
    if (err) {
      result.send(err);
    }
    //result.text = text;
    res.send({ message: 'modification effectue avec succes' });
  })
});*/


router.post('/tokenValidity/:accountAddress/:tokenDuration/:txhash', function (req, res) {
  let user = {};
  user.tokenDate = Date.now();
  user.accountAddress = req.params.accountAddress;
  user.tokenDuration = req.params.tokenDuration;
  user.txhash = req.params.txhash;
  
  let query = { email: session.user.email }

  User.update(query, user, function (err) {

    if (err) {
      console.log(err);
      return;
    } else {
      console.log('token validity/address ')
      res.send("updated");
    }
  });

});

router.get('/checkTokenValidity', function (req, res) {

  /*console.log('validityDate: ' + validityDate);
  console.log("token date:   " + tokenDate);
  console.log("today date:   " + compareDate);

  console.log('token data + 2 moths ' + tokenDate2months);*/
  /* if(tokenDate.getTime()< validityDate.getTime()){
     console.log('token date little than today+2months');
   }
   else{
     console.log('token date bigger than today+2months')
   }*/

  if (req.cookies.user_sid != undefined) {
    if (req.session.user.accountAddress != undefined) {
      var compareDate = new Date(req.session.user.tokenDate);
      var tokenDate = new Date(req.session.user.tokenDate);
      var tokenDate2months = compareDate.setMonth(compareDate.getMonth() + 2);
      var validityDate = new Date(tokenDate2months);
      //res.render('outfit_service');
      // if (tokenDate < validityDate) {
      res.render('outfit_service');
      //  }
      /* else{
         console.log('date ghalta')
       }*/

    }
    else {
      res.render('outfitadviser');
    }
  }
  else if (req.cookies.user_sid == undefined) {
    console.log('not connected')
    res.render('outfitadviser');
  }
});

router.get('/paymentStats', function (req, res, next) {

  var token7 = [];
  var token30 = [];
  var token90 = [];

  User.find({}, 'tokenDuration', function (err, tokenDurations) {
    if (err) {
      res.json(err);
    }
    else {
      tokenDurations.forEach(element => {
        if (element.tokenDuration == 7) {
          token7.push(element.tokenDuration);
        }
        if (element.tokenDuration == 30) {
          token7.push(element.tokenDuration);
        }
        if (element.tokenDuration == 90) {
          token7.push(element.tokenDuration);
        }
      });
      var finalresult = {
        token7: token7.length,
        token30: token30.length,
        token90: token90.length
      }
      res.send(finalresult);
    }
  })
});

router.get('/refreshLocalStorage', function (req, res, next) {
  let user = {};
  user = session.user;
  console.log(session.user);

  User.findOne({ _id: user._id },
    function (err, user) {
      if (err) { res.send(err); }
      else {
        res.json(user);
      }

    })

});

router.get('/monthJoinedStats', function (req, res, next) {

  User.aggregate([
    { $project: { month_joined: { $month: "$tokenDate" } } },
    { $group: { _id: { month_joined: "$month_joined" }, number: { $sum: 1 } } },
    { $sort: { "_id.month_joined": 1 } }
  ], function (err, users) {
    res.json(users);
  });

});

// process the signup form
router.post('/signup', passport.authenticate('local-signup', {
  failureFlash : true // allow flash messages
}));

router.get('/userById/:id', function (req, res) {
  var id = req.params.id;
  User.findById(id).exec(function (err, user) {
      if (err) {
          res.json(err);
      }
      if (!user) {
          res.status(404).send();

      } else {
          res.json(user);
      }
  });
})


module.exports = router;
