var express = require("express");
var router = express.Router();
var Outfit = require('../models/outfit_item');
var InstagramOutfit = require('../models/instagram_outfit_data');
var session = require('express-session');

var nameArray = new Array();
var outfitsuggestion = new Array();
async function syncoutfit(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array)
    }
}

router.post('/', function(req, res) {
    var outfit = new Outfit(req.body)
    console.log(JSON.stringify(outfit));
    outfit.save(function(err, result) {
        if (err) {
            res.send(err);
        }
        res.json({ message: "Successfully posted" })
    })
})

router.get('/', function(req, res) {
    Outfit.find({}, function(error, result) {
        if (error) {
            res.send(error);
        }
        if (!result) {
            res.status(404).send();
        } else {
            res.json(result);
        }
    });
})

router.put('/:id', function(req, res) {
    var id = req.params.id;
    Outfit.findOneAndUpdate({ '_id': id }, req.body, function(err, result) {
        if (err) {
            res.send(err);
        }
        res.send({ message: "modifie avec succes" });
    })
})

router.delete("/:id", function(req, res) {
    var id = req.params.id;
    Outfit.findOneAndRemove({ '_id': id }, function(err, result) {
        if (err) {
            res.send(err)
        }
        res.json({ message: "supprimé avec succes" });
    })
})

router.get('/details/:id', function(req, res) {
    var id = req.params.id;
    Outfit.find({ '_id': id }, function(err, result) {
        if (err) {
            res.status(417).send(err);
        }
        if (!result) {
            res.status(404).send("nothing");
        } else {
            res.json(result);
        }
    })
});

router.get('/compare/outfit/:activitie/:temperature', function(req, res) {
    var activitie = req.params.activitie;
    var temperature = req.params.temperature;
    console.log("id: " + session.user._id)
    Outfit.find({

            $and: [
                    { "activities": { $regex: ".*" + activitie + ".*" } },
                    { "minTemperature": { $lte: temperature } },
                    { "maxTemperature": { $gte: temperature } }
                ]
                /*
                            $and: [
                                { "minAge": { $lte: 24 } },
                                { "maxAge": { $gte: 24 } },
                                { "minTemperature": { $lte: 15 } },
                                { "maxTemperature": { $gte: 15 } },
                                { "sex": "Homme" }
                            ]*/
        },
        function(error, result) {
            if (error) {
                res.send(error);
            }
            if (!result) {
                res.status(404).send();
            } else {
                result.forEach(element => {
                    nameArray.push(element.name)
                });
                console.log(nameArray)
                nameArray.forEach(element => {
                    InstagramOutfit.find({
                            $and: [{
                                    "clothes": { $regex: ".*" + element + ".*" }
                                },
                                { "user": session.user._id }
                            ]
                        },
                        function(err, resultat) {
                            if (err) {
                                res.send(error);
                            }
                            if (!resultat) {
                                res.status(404).send();
                            } else {
                                console.log("la " + resultat)
                                Outfit.find({ "name": element }, function(errorfinal, resultfinal) {
                                    if (errorfinal) {
                                        res.send(errorfinal);
                                    }
                                    if (!resultfinal) {
                                        res.status(404).send();
                                    } else {
                                        outfitsuggestion.push(resultfinal)
                                    }
                                });

                            }
                            //}
                        })
                })
                console.log("1 " + outfitsuggestion)
                res.send(outfitsuggestion)
                nameArray = []
                outfitsuggestion = []
            }
        });

})


module.exports = router;