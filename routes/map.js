var express = require('express');
var router = express.Router();
var request = require('request');

router.get('/:lat/:lng', function (req, res) {
    var lat = req.params.lat;
    var lng = req.params.lng;
    var uri = 'https://api.darksky.net/forecast/762fc653f0bb54a60721409f930be420/' + lat + ',' + lng;
    request(uri, function (error, response, body) {
        console.log('error:', error);
        console.log('statusCode:', response && response.statusCode);
        //console.log('body:', body);
        var responseJson = JSON.parse(body);
        res.json(responseJson);
    });

});

module.exports = router;