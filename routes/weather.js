var express = require('express');
var router = express.Router();
var Station = require('../models/station');
var Weather = require('../models/weather');


router.get('/', function (req, res) {
    Weather.find().exec(function (err, weather) {
        if (err) {
            res.send(err);
        }
        else {
            res.send(weather);
            console.log(weather);
        }
    })
})

router.post('/commitWeather', function (req, res) {
    var weather = new Weather(req.body);
    weather.save(function (err, newcommit) {
        if (err) {
            res.json("weather commited");
        }
        console.log(newcommit);
        res.json({ message: "successfully commited" });
    })
});

router.get('/weatherByStation/:stationId', function (req, res) {
    var stationId = req.params.stationId;
    console.log(stationId)
    Weather.getWeatherByStation(req.params.stationId, function (err, weather) {
        if (err) {
            res.send(err);
        }

        else
            res.send(weather);
    })

})

module.exports = router;
