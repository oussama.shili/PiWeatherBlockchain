var express = require('express');
var router = express.Router();
var Station = require('../models/station');
var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'oumayma.gader@esprit.tn',
        pass: 'twin2017'
    }
});





router.get('/', function (req, res) {
    Station.find().exec(function (err, stations) {
        if (err) {
            res.send(err);
        }
        else {
            res.send(stations);
            console.log(stations);
        }
    })
})

router.post('/addStation', function (req, res) {

   // var mailOptions = {
   //     from: 'oumayma.gader@esprit.tn',
   //     to: 'myfriend@yahoo.com',
   //     subject: 'Sending Email using Node.js',
   //     text: 'That was easy!'
   // };


    var station = new Station(req.body);
    station.save(function (err, newstation) {
        if (err) {
            res.json("station not added");
        }
       // transporter.sendMail(mailOptions, function (error, info) {
       //     if (error) {
       //         console.log(error);
       //     } else {
       //         console.log('Email sent: ' + info.response);
       //     }
       // });
        console.log(newstation);
        res.json({ message: "successfully added" });
    })
});

router.get('/stationById/:id', function (req, res) {
    var id = req.params.id;
    Station.findById(id).populate('createdBy').exec(function (err, station) {
        if (err) {
            res.json(err);
        }
        if (!station) {
            res.status(404).send();

        } else {
            res.json(station);
        }
    });
})


router.get('/stationByUser/:userId', function (req, res) {
    var userId = req.params.userId;
    console.log(userId)
    Station.findStationByUser(req.params.userId, function (err, stations) {
        if (err) {
            res.send(err);
        }

        else
            res.send(stations);
    })

})

router.put('/:id', function (req, res) {
    var id = req.params.id;
    console.log(id)
    Station.findOneAndUpdate({ '_id': id }, req.body, function (err, result) {
        if (err) {
            res.send(err);
        }
        res.send({ message: "modifie avec succes" });
    })
})


router.delete('/:id', function (req, res) {
    var id = req.params.id;
    Station.findOneAndRemove({ '_id': id }, req.body, function (err, result) {
        if (err) {
            res.send(err);
        }
        res.send({ message: "supprime avec succes" });
    })
})

module.exports = router;
